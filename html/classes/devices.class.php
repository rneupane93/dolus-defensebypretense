<?php
	require_once('connect.class.php');
	
	// devices
	class devices{

		// Get All Policies
		public function getAllDevices(){
			$dbcon= new connect();
			$qry=$dbcon->db1->prepare("SELECT * FROM devices");
			$qry->execute();
			$res = $qry->fetchAll(PDO::FETCH_ASSOC); 
			return $res;
		}
	}

?>